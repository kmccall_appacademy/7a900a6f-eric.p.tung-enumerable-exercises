require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr == []
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  result = true
  long_strings.each { |word| result = false if word.include?(substring)==false }
  result
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  counter = Hash.new(0)
  result = []
  string.gsub(/[^'a-zA-Z']/,"").chars.each { |char| counter[char]+=1 }
  counter.each { |k, v| result << k if v > 1 }
  result.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.split(" ").sort_by { |word| word.length }[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = "abcdefghijklmnopqrstuvwxyz".chars
  alphabet.reject { |x| string.include?(x) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select { |year| not_repeat_year?(year) }
end

def not_repeat_year?(year)
  year.to_s.chars.uniq == year.to_s.chars
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  result = songs
  songs.each_with_index { |song, week| songs.delete(song) if week !=0 && songs[week-1] == song }
  result.uniq
end

# def no_repeats?(song_name, songs)
# end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  min_c = 1.0/0.0
  min_c_word = ""
  string.gsub(/[^'a-zA-Z']/," ").split(" ").each do |word|
    temp_c_dist = c_distance(word)
    if temp_c_dist < min_c
      min_c = temp_c_dist
      min_c_word = word
    end
  end
  min_c_word
end


def c_distance(word)
  result = word.reverse.chars.find_index('c')
  (result.nil? ? 1.0/0.0 : result)
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  result = []
  temp_entry=[]
  status = 0  #0 = no match found yet, 1 = there's a match, trying to find a range
  arr.each_with_index do |num, idx|
    case status
    when 0
      if num == arr[idx-1] && idx!=0
        temp_entry << idx-1
        status = 1
      end
    when 1
      if num != arr[idx-1] && idx !=0
        temp_entry << idx-1
        status = 0
        result << temp_entry
        temp_entry = []
      end
    end
  end
  # add the last entry if applicable
  if temp_entry !=[]
    temp_entry << arr.length-1
    result << temp_entry
  end
  result
end
